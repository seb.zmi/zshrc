#!/bin/zsh
# vim: set filetype=zsh

function preexec {
    case $TERM in
    xterm*)
        print -Pn "\e]0;$1\a"
        ;;
    screen)
        #PR_TITLEBAR=$'%{\e_screen \005 (\005t) | %(!.-=[ROOT]=- | .)%n@%m:%~ | ${COLUMNS}x${LINES} | %y\e\\%}'
        ;;
    *)
        PR_TITLEBAR=''
        ;;
    esac
}

extract ()
{
    echo Extracting $1 ...
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xjf $1        ;;
            *.tar.gz)    tar xzf $1     ;;
            *.bz2)       bunzip2 $1       ;;
            *.rar)       unrar x $1     ;;
            *.gz)        gunzip $1     ;;
            *.tar)       tar xf $1        ;;
            *.tbz2)      tar xjf $1      ;;
            *.tbz)       tar -xjvf $1    ;;
            *.tgz)       tar xzf $1       ;;
            *.zip)       unzip $1     ;;
            *.Z)         uncompress $1  ;;
            *.7z)        7z x $1    ;;
            *)           echo "I don't know how to extract '$1'..." ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

pk ()
{
    echo "Archiving $1 ..."
    if [ $1 ] ; then
        case $1 in
            tbz)       tar cjvf $2.tar.bz2 $2      ;;
            tgz)       tar czvf $2.tar.gz  $2       ;;
            tar)      tar cpvf $2.tar  $2       ;;
            bz2)    bzip $2 ;;
            gz)        gzip -c -9 -n $2 > $2.gz ;;
            zip)       zip -r $2.zip $2   ;;
            7z)        7z a $2.7z $2    ;;
            *)         echo "'$1' cannot be packed via pk()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

function zsh_stats() {
  fc -l 1 | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl | head -n20
}

function paste() {
  if [ -z $1 ]; then
    1=text
  fi

  a=$(<&0)
  if [ -z $2 ]; then
    2=1
  fi

  curl -d lang=$1 -d private=$2 -d name=Viperoo -d text="$(echo $a)" http://p.szmijewski.pl/api/create
}


function paste() {
  a=$(<&0)
  a=$(echo $a | sed -e 's/&/%26/g')
  if [ -z $2 ]; then
    2=1
  fi

  if [ -z $1 ]; then
    1=text
  fi

  curl -d lang=$1 -d private=$2 -d name=Viperoo --data-ascii text="$(echo $a)" "http://p.szmijewski.pl/api/create"
}
